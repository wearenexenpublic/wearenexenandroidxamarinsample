﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Support.V4.Content;

namespace WeAreNexenAndroidSample
{
    public class MainActivity : Activity, ActivityCompat.IOnRequestPermissionsResultCallback
    {

        protected override void OnCreate(Android.OS.Bundle savedInstanceState)
        {
;
            base.OnCreate(savedInstanceState);

            GetLocationPermission();

            SetContentView(Resource.Layout.Main);

            ProgressDialog progress = new ProgressDialog(this);
            progress.Indeterminate = true;
            progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            progress.SetMessage("Scanning...");
            progress.SetCancelable(false);
            progress.Show();
        }

        private const int RequestLocationId = 0;

        private void GetLocationPermission()
        {
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.AccessCoarseLocation) != (int)Permission.Granted)
            {
                BeaconApplication.Manager.Stop();

                //Finally request permissions with the list of permissions and Id
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.AccessCoarseLocation }, RequestLocationId);
            }

        }

        void ActivityCompat.IOnRequestPermissionsResultCallback.OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (grantResults[0] == (int) Permission.Granted)
            {
                BeaconApplication.Manager.Start();
            }
        }

    }
}
