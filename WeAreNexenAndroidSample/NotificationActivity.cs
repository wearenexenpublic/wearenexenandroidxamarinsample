﻿using Android.App;
using Android.OS;
using Android.Widget;
using BE.Wearenexen.Network;
using BE.Wearenexen.Network.Callback;
using BE.Wearenexen.Network.Response.Beacon;
using BE.Wearenexen.Network.Response.Beacon.Content;
using Java.Lang;

namespace WeAreNexenAndroidSample
{
    [Activity(Label = "Notification Activity")]
    public class NotificationActivity : Activity, IResponseListener
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Get the count value passed to us from MainActivity:
            var beaconId = Intent.Extras?.GetLong(BE.Wearenexen.Constants.BeaconIdIntentDataKey, long.MinValue);
            var contentId = Intent.Extras?.GetLong(BE.Wearenexen.Constants.ContentIdIntentDataKey, long.MinValue);

            // Display the count sent from the first activity:
            SetContentView(Resource.Layout.Notification);

            if (beaconId > long.MinValue && contentId > long.MinValue)
            {
                ShowContent(beaconId.Value, contentId.Value);
            }
        }

        private void ShowContent(long beaconId, long contentId)
        {
            BeaconApplication.Manager.GetBeaconZone(new Long(beaconId), new Long(contentId), this);
        }


        public void OnError(APIError p0)
        {
        }

        public void OnResponse(Object p0)
        {
            var zone = p0 as BaseNexenZone;
            var content = zone?.Content as HtmlContent;
            if (content != null)
            {
                FindViewById<TextView>(Resource.Id.textView).Text = $"The following url should be shown here: {content.PageUrl} ";
            }
        }
    }
}